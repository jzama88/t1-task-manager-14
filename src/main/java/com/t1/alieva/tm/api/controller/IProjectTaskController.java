package com.t1.alieva.tm.api.controller;

public interface IProjectTaskController {

    void bindTaskToProject();

    void unbindTaskToProject();

}
