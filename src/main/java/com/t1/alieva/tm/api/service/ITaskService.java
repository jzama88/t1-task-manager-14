package com.t1.alieva.tm.api.service;

import com.t1.alieva.tm.enumerated.Sort;
import com.t1.alieva.tm.enumerated.Status;
import com.t1.alieva.tm.model.Project;
import com.t1.alieva.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    List<Task> findAll(Sort sort);

    List<Task> findAll(Comparator comparator);

    Task add(Task task);

    Task create(String name);

    Task create(String name, String description);

    void clear();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    void remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Task changeTaskStatusByIndex(Integer index, Status status);

    Task changeTaskStatusById(String id, Status status);

    List<Task> findAllByProjectId(String projectId);

}
