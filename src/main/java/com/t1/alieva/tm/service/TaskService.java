package com.t1.alieva.tm.service;

import com.t1.alieva.tm.api.repository.ITaskRepository;
import com.t1.alieva.tm.api.service.ITaskService;
import com.t1.alieva.tm.enumerated.Sort;
import com.t1.alieva.tm.enumerated.Status;
import com.t1.alieva.tm.model.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(final Sort sort) {
        if(sort == null) return findAll();
        return findAll(sort.getComparator());
    }

    @Override
    public List<Task> findAll(final Comparator comparator) {
        if(comparator == null) return findAll();
        return taskRepository.findAll(comparator);
    }

    @Override
    public Task add(final Task task) {
        if(task == null) return null;
        return taskRepository.add(task);
    }
    @Override
    public Task create(final String name){
        if(name == null || name.isEmpty()) return null;
        return add(new Task(name));
    }
    @Override
    public Task create(final String name, String description){
        if(name == null || name.isEmpty()) return null;
        if(description == null || description.isEmpty()) return null;
        return add(new Task(name, description));
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task findOneById(final String id){
        if(id == null || id.isEmpty()) return null;
        return taskRepository.findOneById(id);
    }

    @Override
    public Task findOneByIndex(final Integer index){
        if(index == null || index < 0) return null;
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task updateById(final String id, final String name, final String description){
        if(id == null || id.isEmpty()) return null;
        if(name == null || name.isEmpty()) return null;
        final Task task = findOneById(id);
        if(task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description){
        if(index == null || index < 0) return null;
        if(name == null || name.isEmpty()) return null;
        final Task task = findOneByIndex(index);
        if(task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public void remove(final Task task){
        if(task == null) return;
        taskRepository.remove(task);
    }

    @Override
    public Task removeById(final String id){
        if(id == null || id.isEmpty()) return null;
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByIndex(final Integer index){
        if(index == null || index < 0) return null;
        return taskRepository.removeByIndex(index);
    }
    @Override
    public Task changeTaskStatusByIndex(final Integer index, final Status status){
        if (index == null || index < 0) return null;
        if (index >= taskRepository.getSize()) return null;
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String id, final Status status){
        if (id == null || id.isEmpty()) return null;
        final Task task = findOneById(id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId){
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);
    }
}
